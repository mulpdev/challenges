import binascii
import sys

binary_v514 = ['0b01000110', '0b01101100', '0b01100001', '0b01100111', '0b00111101', '0b00110001', '0b00110010', '0b01000101', '0b01011010']
binary_gop  = ['0b01001011', '0b01100101', '0b01111001', '0b00111101', '0b00110010', '0b01000101', '0b01011010', '0b00110000', '0b00110001']


for b in binary_gop:
	integer = int(b, 2)
	sys.stdout.write(binascii.unhexlify('%x' % integer))

sys.stdout.flush()
