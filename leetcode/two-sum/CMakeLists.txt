cmake_minimum_required(VERSION 3.8)
project(leetcode_solutions)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)
add_executable(leetcode_solutions ${SOURCE_FILES})